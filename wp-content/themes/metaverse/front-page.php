<!-- WEBSITE HEADER STARTS HERE -->
    <?php get_header();?>
<!-- WEBSITE HEADER ENDS HERE -->

<!-- PAGE CONTENT STARTS HERE -->

    <section class="section1" id="section1" data-aos="fade-in">
        <div class="my_container">
            <div class="row">
                <div class="col-lg-7 p-0">
                    <div class="section1_title">
                        <h1>
                            <?php the_field('section_1_title'); ?>
                        </h1>
                    </div>
                    <div class="section1_desc">
                        <h2>
                            <?php the_field('section_1_subtitle'); ?>
                        </h2>
                    </div>
                    <div class="metaverse_cta">
                        <a href="javascript:void(0)">
                            Learn more
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 p-0"></div>
                <div class="col-12 text-center">
                    <div class="section1_scroll_down">
                        <a class="scrolls" href="#section2">
                            <i class="fa fa-angle-down"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section2" id="section2" data-aos="fade-in">
        <div class="my_container">
            <div class="section2_inner">
                <div class="section2_videobox">
                    <div class="videoContainer">
                        <iframe class="iframes" id="section2_video" width="100%" height="100%" src="https://www.youtube.com/embed/EngW7tLk6R8?rel=0&amp;autoplay=1&amp;mute=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                        <div id="thumbContainer1" class="thumbContainer text-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/section2_video_cover.jpg" class="img-fluid">
                            <img onclick="playVideo();" src="<?php echo get_template_directory_uri(); ?>/assets/images/playicon.png" class=" playIcon">
                        </div>
                    </div>
                </div>
                <div class="section2_databox">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-7 p-0">
                                <div class="section2_about redbg">
                                    <div class="section_title">
                                        <h3>
                                            <?php the_field('about_metaverse_title'); ?>
                                        </h3>
                                    </div>
                                    <div class="section_desc">
                                        <h4>
                                            <?php the_field('about_metaverse_subtitle'); ?>
                                        </h4>
                                    </div>
                                    <?php the_field('about_metaverse_desc'); ?>
                                </div>
                            </div>
                            <div class="col-lg-5 p-0">
                                <div class="section_hiw">
                                    <div class="section_title">
                                        <h3>
                                            <?php the_field('how_it_works_title'); ?>
                                        </h3>
                                    </div>
                                    <div class="section_desc">
                                        <h4>
                                            <?php the_field('how_it_works_subtitle'); ?>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section2_hiw_details">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="hiw_detail">
                                    <div class="hiw_img">
                                        <img src="<?php the_field('how_it_works_image'); ?>" class="img-fluid">
                                    </div>
                                    <div class="hiw_title">
                                        RIDE
                                    </div>
                                    <div class="hiw_desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="hiw_detail">
                                    <div class="hiw_img">
                                        <img src="<?php the_field('how_it_works_image_2'); ?>" class="img-fluid">
                                    </div>
                                    <div class="hiw_title">
                                        FIGHT
                                    </div>
                                    <div class="hiw_desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="hiw_detail">
                                    <div class="hiw_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/metaverse_placeholder_join_a_gang-01.png" class="img-fluid">
                                    </div>
                                    <div class="hiw_title">
                                        JOIN A GANG
                                    </div>
                                    <div class="hiw_desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="hiw_detail">
                                    <div class="hiw_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/metaverse_placeholder_equipement-01.png" class="img-fluid">
                                    </div>
                                    <div class="hiw_title">
                                        UPGRADE EQUIPMENT
                                    </div>
                                    <div class="hiw_desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section3" id="section3" data-aos="fade-in">
        <div class="my_container p-0">
            <div class="section3_inner redbg">
                <div class="section_title">
                    <h3>
                        Latest nft’s
                    </h3>
                </div>
                <div class="section_desc">
                    <h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </h4>
                </div>
                <div class="nft_slider">
                    <div class="nft_swiper swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="nft_box">
                                    <div class="nft_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nft1.jpg" class="img-fluid">
                                        <span class="nft_likes">
                                            <i class="fa fa-heart"></i>
                                            99
                                        </span>
                                    </div>
                                    <div class="nft_details">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-6 p-0">
                                                    <div class="text-left">
                                                        <div class="nft_series">
                                                            Editor Series
                                                        </div>
                                                        <div class="nft_name">
                                                            Pinkshark
                                                        </div>
                                                        <div class="nft_serial">
                                                            #090887
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6 p-0">
                                                    <div class="text-right">
                                                        <div class="nft_bid_type">
                                                            Top Bid
                                                        </div>
                                                        <div class="nft_eth">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/eth_icon.svg" class="img-fluid"> 2.99 ETH
                                                        </div>
                                                        <div class="nft_days_left">
                                                            1 day left
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="nft_box">
                                    <div class="nft_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nft2.jpg" class="img-fluid">
                                        <span class="nft_likes">
                                            <i class="fa fa-heart"></i>
                                            99
                                        </span>
                                    </div>
                                    <div class="nft_details">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-6 p-0">
                                                    <div class="text-left">
                                                        <div class="nft_series">
                                                            Editor Series
                                                        </div>
                                                        <div class="nft_name">
                                                            Pinkshark
                                                        </div>
                                                        <div class="nft_serial">
                                                            #090887
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6 p-0">
                                                    <div class="text-right">
                                                        <div class="nft_bid_type">
                                                            Top Bid
                                                        </div>
                                                        <div class="nft_eth">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/eth_icon.svg" class="img-fluid"> 2.99 ETH
                                                        </div>
                                                        <div class="nft_days_left">
                                                            1 day left
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="nft_box">
                                    <div class="nft_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nft3.jpg" class="img-fluid">
                                        <span class="nft_likes">
                                            <i class="fa fa-heart"></i>
                                            99
                                        </span>
                                    </div>
                                    <div class="nft_details">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-6 p-0">
                                                    <div class="text-left">
                                                        <div class="nft_series">
                                                            Editor Series
                                                        </div>
                                                        <div class="nft_name">
                                                            Pinkshark
                                                        </div>
                                                        <div class="nft_serial">
                                                            #090887
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6 p-0">
                                                    <div class="text-right">
                                                        <div class="nft_bid_type">
                                                            Top Bid
                                                        </div>
                                                        <div class="nft_eth">
                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/eth_icon.svg" class="img-fluid"> 2.99 ETH
                                                        </div>
                                                        <div class="nft_days_left">
                                                            1 day left
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-next"><i class="fa fa-chevron-right arrows"></i></div>
                    <div class="swiper-button-prev"><i class="fa fa-chevron-left arrows"></i></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section4" id="section4" data-aos="fade-in">
        <div class="my_container text-center">
            <div class="section_title">
                <h3>
                    Join with our community
                </h3>
            </div>
            <div class="section_desc">
                <h4>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </h4>
            </div>
            <div class="joc_media">
                <a href="javascript:void(0)"><i class="fa fa-facebook"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-twitter"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-paper-plane"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-instagram"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-simplybuilt"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-maxcdn"></i></a>
            </div>
        </div>
    </section>
    <section class="section5" id="section5" data-aos="fade-in">
        <div class="my_container">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-4">
                    <div class="section_title">
                        <h3>
                            tokenomics
                        </h3>
                    </div>
                    <div class="section_desc">
                        <h4>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </h4>
                    </div>
                    <div class="metaverse_cta">
                        <a href="javascript:void(0)">
                            Download
                        </a>
                    </div>
                </div>
                <div class="col-lg-2"></div>
                <div class="col-lg-4">
                </div>
                <div class="col-lg-5 text-center">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/tokenomics_circle.png" class="img-fluid">
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </section>
    <section class="section6" id="section6" data-aos="fade-in">
        <div class="my_container text-center">
            <div class="section_title">
                <h3>
                    <a href="javascript:void(0);" class="tab_a active" data-attr="OurTeam">Our Team</a>
                    <a href="javascript:void(0);" class="tab_a" data-attr="Ouradvisors">Our advisors</a>
                </h3>
            </div>
            <div class="OurTeam tab_data active_tab_data">
                <div class="section_desc">
                    <h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </h4>
                </div>
                <div class="team_details">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Victoria-Mathis.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Victoria Mathis
                                    </div>
                                    <div class="team_member_desg">
                                        CEO
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Theresa-Sanderson.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Theresa Sanderson
                                    </div>
                                    <div class="team_member_desg">
                                        CFO
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Bella-Hunter.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Bella Hunter
                                    </div>
                                    <div class="team_member_desg">
                                        Creative Manager
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Ahmed-Elsayed.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Ahmed Elsayed
                                    </div>
                                    <div class="team_member_desg">
                                        UX/UI Designer
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="Ouradvisors tab_data">
                <div class="section_desc">
                    <h4>
                        Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </h4>
                </div>
                <div class="team_details">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Bella-Hunter.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Bella Hunter
                                    </div>
                                    <div class="team_member_desg">
                                        Creative Manager
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Victoria-Mathis.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Victoria Mathis
                                    </div>
                                    <div class="team_member_desg">
                                        CEO
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Ahmed-Elsayed.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Ahmed Elsayed
                                    </div>
                                    <div class="team_member_desg">
                                        UX/UI Designer
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="team_member_detail">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Theresa-Sanderson.jpg" class="img-fluid">
                                    <div class="team_member_name">
                                        Theresa Sanderson
                                    </div>
                                    <div class="team_member_desg">
                                        CFO
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section7" id="section7" data-aos="fade-in">
        <div class="my_container text-center">
            <div class="section_title">
                <h3>
                    11metaverse roadmap
                </h3>
            </div>
            <div class="roadmap_slider">
                <div class="roadmap_swiper swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="roadmap_box">
                                <div class="roadmap_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/rm1.jpg" class="img-fluid">
                                </div>
                                <div class="roadmap_data">
                                    <div class="roadmap_quater">
                                        Q1, 2021
                                    </div>
                                    <div class="roadmap_topic">
                                        Research
                                    </div>
                                    <div class="roadmap_desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="roadmap_box">
                                <div class="roadmap_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/rm2.jpg" class="img-fluid">
                                </div>
                                <div class="roadmap_data">
                                    <div class="roadmap_quater">
                                        Q2, 2021
                                    </div>
                                    <div class="roadmap_topic">
                                        Testnet
                                    </div>
                                    <div class="roadmap_desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="roadmap_box">
                                <div class="roadmap_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/rm3.jpg" class="img-fluid">
                                </div>
                                <div class="roadmap_data">
                                    <div class="roadmap_quater">
                                        Q3, 2021
                                    </div>
                                    <div class="roadmap_topic">
                                        Mainnet
                                    </div>
                                    <div class="roadmap_desc">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section8" id="section8" data-aos="fade-in">
        <div class="my_container">
            <div class="row">
                <div class="col-lg-6 text-center">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/section1_img.jpg" class="img-fluid">
                </div>
                <div class="col-lg-6">
                    <div class="redbg">
                        <div class="section_title">
                            <h3>
                                Get 11metaverse subscriptions
                            </h3>
                        </div>
                        <div class="section_desc">
                            <h4>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </h4>
                        </div>
                        <div class="subscription_form">
                            <span>I am a..</span>
                            <form id="subscription_form" action="javascript:void(0)">
                                <div class="form-group">
                                    <select name="cars" class="custom-select" required>
                                        <option value="Game Developer">Game Developer</option>
                                        <option value="IOS Developer">IOS Developer</option>
                                        <option value="Android Developer">Android Developer</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="email" class="form-control" placeholder="Email" required>
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" >SUBMIT</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-check">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                                        <label class="custom-control-label" for="customCheck">I accept the <a href="<?php echo get_bloginfo('url') ?>/terms-and-conditions/">terms & conditions</a></label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section9" id="section9" data-aos="fade-in">
        <div class="my_container text-center">
            <div class="section_title">
                <h3>
                    Partners
                </h3>
            </div>
            <div class="section_desc">
                <h4>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </h4>
            </div>
            <div class="partner_slider">
                <div class="partner_swiper swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="partner_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Component41-1.png" class="img-fluid">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="partner_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Component42-1.png" class="img-fluid">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="partner_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Component41-1.png" class="img-fluid">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="partner_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Component42-1.png" class="img-fluid">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="partner_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Component41-1.png" class="img-fluid">
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="partner_img">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Component42-1.png" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next"><i class="fa fa-chevron-right arrows"></i></div>
                <div class="swiper-button-prev"><i class="fa fa-chevron-left arrows"></i></div>
            </div>
        </div>
    </section>
<!-- PAGE CONTENT ENDS HERE -->
    
<!-- WEBSITE FOOTER STARTS HERE -->
    <?php get_footer();?>
<!-- WEBSITE FOOTER ENDS HERE