            <?php 
            wp_footer()
            ?>
            <div class="fixed_social_media">
                <a href="javascript:void(0)"><i class="fa fa-twitter"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-facebook"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-maxcdn"></i></a>
                <a href="javascript:void(0)"><i class="fa fa-simplybuilt"></i></a>
            </div>
            <footer>
                <div class="my_container">
                    <div class="footer_logo text-center">
                        <a href="javascript:void(0);" class="d-inline-block">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footerlogo.svg" class="img-fluid">
                        </a>
                    </div>
                    <div class="copyright_line">
                        <span>
                            Copyright ©2022, 
                            <b>
                                11Metaverse
                            </b>
                        </span>
                        <span>
                            <a href="javascript:void(0)">
                                Terms & Conditions
                            </a>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <a href="javascript:void(0)">
                                Privacy Policy
                            </a>
                        </span>
                        <span>
                            Crafted by 
                            <a href="javascript:void(0);">
                                Crowd
                            </a>
                        </span>
                    </div>
                </div>
            </footer>
            <script type="text/javascript">
                function playVideo() {
                    document.getElementById("thumbContainer1").style.display='none'
                    $("#section2_video")[0].src += "?autoplay=1";
                    document.getElementById("section2_video").style.display='block'  
                }
            </script>
        </body>
</html>