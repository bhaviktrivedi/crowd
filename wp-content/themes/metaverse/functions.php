<?php
	function metaverse_theme_support(){
		//Adds dynamic title tag support
		add_theme_support('title-tag');
	}

	add_action('after_setup_theme','metaverse_theme_support');

	function metaverse_register_styles(){
		wp_enqueue_style('metaverse_bootstrap', "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css", array(), '1.0', 'all');
		wp_enqueue_style('metaverse_swiper', "https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css", array(), '1.0', 'all');
		wp_enqueue_style('metaverse_fontawesome', "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css", array(), '1.0', 'all');
		wp_enqueue_style('metaverse_aos', "https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css", array(), '1.0', 'all');
		wp_enqueue_style('metaverse_animate', "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css", array(), '1.0', 'all');
		wp_enqueue_style('metaverse_fonts', get_template_directory_uri() . "/assets/css/fonts.css", array('metaverse_bootstrap'), '1.0', 'all');
		wp_enqueue_style('metaverse_header-footer', get_template_directory_uri() . "/assets/css/header-footer.css", array('metaverse_bootstrap'), '1.0', 'all');
	}

	add_action('wp_enqueue_scripts', 'metaverse_register_styles');


	function metaverse_register_scripts(){
		wp_enqueue_script('metaverse_js', "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js", array(), '1.0', true);
		wp_enqueue_script('metaverse_popper', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js", array(), '1.0', true);
		wp_enqueue_script('metaverse_bootstrap', "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js", array(), '1.0', true);
		wp_enqueue_script('metaverse_jquery-ui', "https://code.jquery.com/ui/1.12.1/jquery-ui.js", array(), '1.0', true);
		wp_enqueue_script('metaverse_Swiper', "https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js", array(), '1.0', true);
		wp_enqueue_script('metaverse_Aos', "https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js", array(), '1.0', true);
		wp_enqueue_script('metaverse_common', get_template_directory_uri() . "/assets/js/common.js", array(), '1.0', true);
	}

	add_action('wp_enqueue_scripts', 'metaverse_register_scripts');
?>