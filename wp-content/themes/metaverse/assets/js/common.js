// AOS.init({disable: 'mobile'});
AOS.init();

$('.scrolls').click(function(evt) { 
 	var $anchor = $(this); 
 	$('html, body').stop().animate({ 
 		scrollTop: $($anchor.attr('href')).offset().top-91
 	}, 1000);
 	evt.preventDefault(); 
});

$(document).on('click','header .menu', function(e){
    $('body').toggleClass('bodyoverflow')
});

$(document).on('click','.nav-item .nav-link', function(e){
    $('header .nav-link').removeClass('active');
    $(this).addClass('active');
    $('header .menu').removeClass('opened');
    $('header .navbar-collapse').removeClass('show');
    $('body').removeClass('bodyoverflow');
});

$(document).on('click','.section_title h3 a', function(e){
    $('.section6 .section_title h3 a').removeClass('active');
    $(this).addClass('active');
    var active_tab = $(this).attr('data-attr');
    $('.tab_data').removeClass('active_tab_data');
    $('.'+active_tab).addClass('active_tab_data');
});


var swiper = new Swiper('.nft_swiper.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 27,
    slideToClickedSlide: true,
    loop: true,
    loopedSlides: 3,
    navigation: {
        nextEl: ".nft_slider .swiper-button-next",
        prevEl: ".nft_slider .swiper-button-prev",
    },
    breakpoints: {
        834: {
            slidesPerView: 2.25,
            spaceBetween: 15,
            centeredSlides: true,
        },
        767: {
            slidesPerView: 1.25,
            spaceBetween: 15,
            centeredSlides: true,
        },
    }
});

var swiper = new Swiper('.roadmap_swiper.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 27,
    slideToClickedSlide: true,
    loop: true,
    loopedSlides: 3,
    breakpoints: {
        834: {
            slidesPerView: 1,
        }
    }
});

var swiper = new Swiper('.partner_swiper.swiper-container', {
    slidesPerView: 6,
    spaceBetween: 27,
    slideToClickedSlide: true,
    navigation: {
        nextEl: ".partner_slider .swiper-button-next",
        prevEl: ".partner_slider .swiper-button-prev",
    },
    loop: true,
    loopedSlides: 3,
    breakpoints: {
        767: {
            slidesPerView: 2.5,
            spaceBetween: 10,
            centeredSlides: true,
        },
    }
});


if (window.innerWidth > 1194) { // code for large screens


    (function($) {
        $.fn.visible = function(partial) {
            var $t            = $(this),
                $w            = $(window),
                viewTop       = $w.scrollTop(),
                viewBottom    = viewTop + $w.height(),
                _top          = $t.offset().top,
                _bottom       = _top + $t.height(),
                compareTop    = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;
            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
        };
    })(jQuery);

    $(window).scroll(function(event) {
      
        $(".section5").each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                el.addClass("fadeIn"); 
            } else {
                el.removeClass("fadeIn");
            }
        });
    });
}