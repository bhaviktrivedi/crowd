//Video Start
  function playVideo () {
    document.getElementById("videocover").style.display='none'
    $("#walkthroughvideo")[0].src += "?autoplay=1";
    document.getElementById("walkthroughvideo").style.display='block'
  }
//Video End

//Modal Start
  $(document).ready(function(){
    setTimeout(function(){
      $('#enqireModal').modal('show');
    },3000);
  });
//Modal End

//Autocomplete Start
  $(document).ready(function(){ 
  $("input").attr("autocomplete", "off");
  });
//Autocomplete End
  
//PDF Start
  $(document).on('click', ".mypdf", function() {
   var myval = $(this).attr("data-attr");
   $('#clickedPDF').val(myval);
  });
//PDF End

var base_url = $('#base_url').val();
$(document).ready(function() {
  //Footer Form
  $('#enquireform').submit(function (e) {
          e.preventDefault();
          var pattern = /^[\s()+-]*([0-9][\s()+-]*){10,10}$/;
          var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

          var name = $("input[name='enquire_name']").val();
          if (name == '') {
            $('#enquire_name_error').text('This field is required');
            return false;
          }

          var phone = $("#enquire_phone").val();
          if (phone == '') {
            $('#enquire_phone_error').text('This field is required');
            return false;
          }

          if (phone != "") {
            if (!pattern.test(phone)) {
              $('#enquire_phone_error').text('Enter a 10 digit Mobile Number');
              return false;
            }
          }

          /*Email key*/
          var Email = $("#enquire_email").val();
          if (Email == '') {
            $('#enquire_email_error').text('This field is required');
            return false;
          }
          if (Email != "") {
            if (!email_regex.test(Email)) {
              $('#enquire_email_error').text('The e-mail address entered is invalid.');
              return false;
            }
          }
         
          var form = new FormData($('#enquireform')[0]);
          $.ajax({
            url: base_url+'Home/Enquire',
            type: 'POST',
            dataType: "JSON",
            processData: false,
            contentType: false,
            data: form, // serializes the form's elements.
            beforeSend: function () {
              $("#loader").show();
              $("#makemedisable").attr("disabled", true);
            },
            success: function (data) {
              // var json = $.parseJSON(data);
              console.log(data);

              if (data.status == "true") {
                $("#enquireform")[0].reset();
                window.location.href = base_url+"thankyou";
                // window.location.href = "https://transcon-triumph.com/thankyou";
                // $('.ajaxsuccess').text(data.msg);
                // setTimeout(function () {
                //   $('.ajaxsuccess').text('');
                // }, 4000);
              } else {
                $('.ajaxsuccess').text(data.msg);
              }
            },
            complete: function () {
              $("#loader").hide();
              $("#makemedisable").attr("disabled", false);
            }
          });
  });
  //Modal Form
  $('#enquireform-modal').submit(function (e) {
          e.preventDefault();

          var pattern = /^[\s()+-]*([0-9][\s()+-]*){10,10}$/;
          var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

          var name = $("#modal_enquire_name").val();
          if (name == '') {
            $('#modal_enquire_name_error').text('This field is required');
            return false;
          }

          var phone = $("#modal_enquire_phone").val();
          if (phone == '') {
            $('#modal_enquire_phone_error').text('This field is required');
            return false;
          }

          if (phone != "") {
            if (!pattern.test(phone)) {
              $('#modal_enquire_phone_error').text('Enter a 10 digit Mobile Number');
              return false;
            }
          }

          /*Email key*/
          var Email = $("#modal_enquire_email").val();
          if (Email == '') {
            $('#modal_enquire_email_error').text('This field is required');
            return false;
          }
          if (Email != "") {
            if (!email_regex.test(Email)) {
              $('#modal_enquire_email_error').text('The e-mail address entered is invalid.');
              return false;
            }
          }
          //PDF Start
          var checkpdf = $('#clickedPDF').val();

          if(checkpdf == 'rightpdf'){
            var link = $("#rightpdf_click").attr("href");
          }
          if(checkpdf == 'midpdf'){
            var link = $("#midpdf_click").attr("href");
          }

          if(checkpdf == 'midpdf1'){
            var link = $("#midpdf_click1").attr("href");
          }

          if(checkpdf == 'rightpdf1'){
            var link = $("#rightpdf_click1").attr("href");
          }
          //PDF End

          var form = new FormData($('#enquireform-modal')[0]);
          $.ajax({
            url: base_url+'Home/Enquire',
            type: 'POST',
            dataType: "JSON",
            processData: false,
            contentType: false,
            data: form, // serializes the form's elements.
            beforeSend: function () {
              $("#modal_loader").show();
              $("#modal_makemedisable").attr("disabled", true);
            },
            success: function (data) {
               console.log(data);

              if (data.status == "true") {
                $("#enquireform-modal")[0].reset();
                // window.location.href = base_url+"thankyou";
                // window.location.href = "https://transcon-triumph.com/thankyou";
                // $('.modal_ajaxsuccess').text(data.msg);
                // setTimeout(function () {
                //   $('.modal_ajaxsuccess').text('');
                // }, 4000);

                // if(checkpdf){
                //   setTimeout(function () {
                //         window.open(link, '_blank');
                //   }, 3000);
                // }
                // window.open(link, '_blank');
                window.open(link, '_blank');
                window.location.href = base_url+"thankyou";

              } else {
                $('.modal_ajaxsuccess').text(data.msg);
              }
            },
            complete: function () {
              $("#modal_loader").hide();
              $("#modal_makemedisable").attr("disabled", false);
            }
          });
  });
});

//Error Remove Start
  $( "input" ).focus(function() {
    $( ".error" ).text('');
  }); 

  $( "select" ).focus(function() {
    $( ".error" ).text('');
  });
//Error Remove End

//Check Number Start
  function isNumberKey(evt){
      var charCode = (evt.which) ? evt.which : evt.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }
//Check Number End
 