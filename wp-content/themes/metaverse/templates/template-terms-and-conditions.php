<?php 
/*
Template Name: Terms and Conditions
*/
get_header();?>

<section>
    <div class="my_container">
        <h1><?php the_title(); ?></h1>
        <?php 
            if( have_posts() ){
                while( have_posts() ){
                    the_post();
                    get_template_part('template-parts/content','page');
                }
            }
        ?>
    </div>    
</section>

<?php get_footer();?>